<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'author', 'created_at'
    ];

    /**
     * Get the user that owns the entry.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'author', 'id');
    }
}
