<?php

namespace App\Http\Controllers;

use App\User;
use App\Tweet;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class EntryUserController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::whereId($id)->with('entries')->first();
        $hiddenTweets = Tweet::where('author', $id)->where('is_hidden', 1)->get()->toArray();

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer AAAAAAAAAAAAAAAAAAAAAPNRCAEAAAAAU8rTyhD%2FkQ3S0yWr1dTvnXC5yhY%3DmPLGYNBGmGo9QgqOIqfid8viNj1zfbcPj9j7D3ZEe4nTImmxvG'
            ]
        ]);

        $tweets = [];
        try {
            $promise = $client->getAsync('https://api.twitter.com/1.1/statuses/user_timeline.json', [
                'query' => [
                    'screen_name' => $user->twitter_username
                ]
            ])->then(
                function ($response) {
                    return  $response->getStatusCode() == 200 ? json_decode($response->getBody(), true) : [];
                },
                function ($exception) {
                    return [];
                }
            );
            $tweets = $promise->wait();
        } catch (RequestException $e) {
            $tweets = [];
        }

        foreach ($tweets as $key => $tweet) {
            $findTweet = array_search($tweet['id'], array_column($hiddenTweets, 'id_tweet'));
            $tweets[$key]['is_hidden'] = gettype($findTweet) == "boolean" ? false : true;
        }

        return view('entries.user.profile', ['user' => $user, 'tweets' => $tweets]);
    }
}
