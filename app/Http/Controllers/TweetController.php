<?php

namespace App\Http\Controllers;

use App\Tweet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TweetController extends Controller
{
    public function hideTweet($idTweet)
    {
        $data['author'] = Auth::user()->id;
        $data['is_hidden'] = 1;
        $data['id_tweet'] = $idTweet;

        Tweet::create($data);

        return response()->json([
            'status' => 200,
            'message' => 'Successfully hidden tweet!'
        ]);
    }

    public function deleteTweet($idTweet)
    {
        $tweet = Tweet::where('id_tweet', $idTweet)->first();
        $tweet->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Successfully show tweet!'
        ]);
    }
}
