<?php

namespace App\Http\Controllers;

use App\Entry;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->has('page') ? $request->input('page') : 1;
        $limitOfRegister = 3;

        $getAllEntries = Entry::with('user')
            ->orderBy('created_at', 'desc')
            ->get()
            ->groupBy('author')
            ->toArray();

        $offset = $page == 1 ? 0 : (($page * 3) - 3);
        $sliceEntries = array_slice($getAllEntries, $offset, $limitOfRegister);

        $entries = new Paginator($sliceEntries, count($getAllEntries), $limitOfRegister, $page,  [
            'path'  => $request->url(),
            'query' => $request->query(),
        ]);

        return view('entries.list', ['entries' => $entries]);
    }
}
