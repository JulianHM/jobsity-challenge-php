<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_hidden', 'author', 'id_tweet'
    ];

    /**
     * Get the user that owns the tweet.
     */
    public function post()
    {
        return $this->belongsTo('App\User', 'author', 'id');
    }
}
