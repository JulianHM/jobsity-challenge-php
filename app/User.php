<?php

namespace App;

use App\Entry;
use App\Tweet;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'twitter_username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the entries for the user.
     */
    public function entries()
    {
        return $this->hasMany(Entry::class, 'author');
    }

    /**
     * Get the tweets for the user.
     */
    public function tweets()
    {
        return $this->hasMany(Tweet::class);
    }
}
