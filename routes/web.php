<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('entries/user/{id}', 'EntryUserController@show');

Route::middleware(['auth'])->group(function () {
    Route::resource('entries', 'EntryController', ['except' => ['destroy', 'show', 'index']]);
    Route::get('tweet/hide/{idTweet}', 'TweetController@hideTweet');
    Route::get('tweet/delete/{idTweet}', 'TweetController@deleteTweet');
});
