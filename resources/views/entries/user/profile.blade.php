@extends('layouts.app')

@section('content')
<div class="container">
    <div class="alert alert-success" style="display:none" role="alert" id="alert"></div>
    <div class="row justify-content-center">
        <div class="col-md-4">
            @foreach ($tweets as $tweet)
            @if($tweet['is_hidden'])
            @auth
            @if(Auth::user()->id == $user['id'])
            <div class="card list-tweets-card">
                <div class="card-body">
                    <h5 class="card-title">{{$user->twitter_username}}</h5>
                    <p class="card-text">{{$tweet['text']}}</p>
                    <hr class="separator">
                    <a href="#" class="showTweet btn-showTweet" name='{{$tweet["id"]}}|show'>Show Tweet ?</a>
                </div>
            </div>
            @endif
            @endauth
            @else
            <div class="card list-tweets-card">
                <div class='card-body'>
                    <h5 class="card-title">{{$user->twitter_username}}</h5>
                    <p class="card-text">{{$tweet['text']}}</p>
                    @auth
                    @if(Auth::user()->id == $user['id'])
                    <hr class="separator">
                    <a href="#" class="hideTweet btn-hideTweet" name='{{$tweet["id"]}}|hide'>Hide Tweet ?</a>
                    @endif
                    @endauth
                </div>
            </div>
            @endif
            @endforeach
        </div>
        <div class="col-md-8">
            @foreach ($user->entries as $entry)
            <div class="card list-entries-card">
                <div class="card-body">
                    <h2 class="card-title list-entries-title">{{$entry->title}}</h2>
                    <p class="card-text">{{$entry->content}}</p>
                    @auth
                    @if(Auth::user()->id == $user['id'])
                    <hr class="separator">
                    <a href="{{url('/entries/'.$entry->id.'/edit')}}" class="btn-filled">Edit Entry</a>
                    @endif
                    @endauth
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="modal fade" id="confirmHideOrShowTweet" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <H3 id="modalQuestion"></H3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">CANCEL</button>
                <button type="button" class="btn btn-danger" id="buttonConfirmationAction"></button>
                <button type="button" class="btn btn-danger" id="loadingButton">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    Loading...
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    jQuery(document).ready(() => {
        // -----------------  Declaration Methods 
        const eventHideTweet = () => {
            $('.hideTweet').click(event => {
                // Get Tweet's id 
                idTweet = getIdTweet(event);
                // Set modal title
                $('#modalTitle').text('HIDE TWEET');
                // Set modal question
                $('#modalQuestion').html('Are you sure you want to <span>HIDE</span> Tweet?');
                // Set text to button 
                $('#buttonConfirmationAction').text('YES, HIDE TWEET');
                // Open modal to confirm action
                $('#confirmHideOrShowTweet').modal('show');
                // Set action
                action = "hide";
            })
        }

        const eventShowTweet = () => {
            $('.showTweet').click(event => {
                // Get Tweet's id 
                idTweet = getIdTweet(event);
                // Set modal title
                $('#modalTitle').text('SHOW TWEET')
                // Set modal question
                $('#modalQuestion').html('Are you sure you want to <span>SHOW</span> Tweet?')
                // Set text to button 
                $('#buttonConfirmationAction').text('YES, SHOW TWEET')
                // Open modal to confirm action
                $('#confirmHideOrShowTweet').modal('show');
                // Set action
                action = "show";
            })
        }

        const getIdTweet = (event) => {
            const nameTweet = $(event.target).attr('name');
            const splitNameTweet = nameTweet.split('|');
            return splitNameTweet[0];
        }

        const hideTweet = (url_global, idTweet) => {
            return $.ajax({
                url: `${url_global}/tweet/hide/${idTweet}`,
                dataType: 'json',
                method: 'get'
            })
        }

        const showTwee = (url_global, idTweet) => {
            return $.ajax({
                url: `${url_global}/tweet/delete/${idTweet}`,
                dataType: 'json',
                method: 'get'
            })
        }

        const showAlertMessage = (message) => {
            $('#alert').show();
            $('#alert').text(message);
            setTimeout(() => {
                $('#alert').hide();
            }, 5000)
        }

        const hideModal = () => {
            $('#confirmHideOrShowTweet').modal('hide');
        }

        // ----------------- End methods

        $('#loadingButton').hide();
        $('#alert').hide()
        const url_global = '{{url("/")}}';
        let idTweet;
        let action;

        // Listen to link hideTweet
        eventHideTweet();

        // Listen to link showTweet
        eventShowTweet();

        $('#buttonConfirmationAction').click((event) => {
            // Hide button confirmation 
            $('#buttonConfirmationAction').hide()
            // Show button loading
            $('#loadingButton').show();

            if (action === 'hide') {
                hideTweet(url_global, idTweet)
                    .done((response) => {
                        // Show alert with message
                        showAlertMessage(response.message);
                        // Close Modal
                        hideModal()
                        $(`a[name ="${idTweet}|hide"]`).parent().append(`<a href="#" class="showTweet btn-showTweet" name='${idTweet}|show'>Show Tweet ?</a`);
                        $(`a[name ="${idTweet}|hide"]`).hide();
                        // Listen event showTweet
                        eventShowTweet();
                    }).always(() => {
                        $('#buttonConfirmationAction').show()
                        $('#loadingButton').hide();
                    })
            } else {
                showTwee(url_global, idTweet)
                    .done((response) => {
                        // Show alert with message
                        showAlertMessage(response.message)
                        // Close Modal
                        hideModal()
                        $(`a[name ="${idTweet}|show"]`).parent().append(`   <a href="#" class="hideTweet btn-hideTweet" name='${idTweet}|hide'>Hide Tweet ?</a>`);
                        $(`a[name ="${idTweet}|show"]`).hide();
                        // Listen event hideTweet
                        eventHideTweet();
                    }).always(() => {
                        $('#buttonConfirmationAction').show()
                        $('#loadingButton').hide();
                    })
            }
        })
    })
</script>
@endsection