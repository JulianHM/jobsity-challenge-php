@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach ($entries as $entry)
            @foreach ($entry as $key => $userEntry)
            @if ($key <= 2) <div class="card list-entries-card">
                <div class="card-body">
                    <h2 class="card-title list-entries-title">{{$userEntry['title']}}</h2>
                    <h6 class="card-subtitle mb-2 text-muted">Author
                        <a href="{{'entries/user/'.$userEntry['user']['id']}}">{{$userEntry['user']['username']}}</a>
                    </h6>
                    <p class="card-text">{{$userEntry['content']}}</p>
                    @auth
                    @if(Auth::user()->id == $userEntry['user']['id'])
                    <hr class="separator">
                    <a href="{{url('/entries/'.$userEntry['id'].'/edit')}}" class="btn-filled">Edit Entry</a>
                    @endif
                    @endauth
                </div>
        </div>
        @endif @endforeach @endforeach
        {{ $entries->links() }}
    </div>
</div>
</div> @endsection